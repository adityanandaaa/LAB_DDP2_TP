package customer;
import theater.*;
import movie.*;
import ticket.*;
import java.util.ArrayList;

public class Customer{
    private String nama;
    private int umur;
    private boolean jenisKelamin;

    public Customer(String nama, boolean jenisKelamin, int umur ){
        this.nama = nama;
        this.umur = umur;
        this.jenisKelamin = jenisKelamin;
    }

    public void setNama(String nama){
        this.nama = nama ;
    }
    public void setUmur(int umur){
        this.umur = umur;
    }
    public void setJenisKelamin(boolean jenisKelamin){
        this.jenisKelamin = jenisKelamin;
    }

    public String getNama(){
        return nama;
    }
    public int getUmur(){
        return umur;
    }
    public boolean getJenisKelamin(){
        return jenisKelamin;
    }
public Ticket orderTicket(Theater bioskop, String judul, String jadwalhari, String jenisFilm){
        for(Ticket ticket : bioskop.getTicket()){
            Movie movie = ticket.getFilm();
            String rating = movie.getRating();


            if (movie.getJudul().equals(judul) && ticket.getHari().equals(jadwalhari) && ticket.getJenisFilm().equals(jenisFilm)){
                if(rating.equals("Umum")){
                    System.out.println(nama+" telah membeli tiket "+judul+" jenis "+jenisFilm+" di "+bioskop.getNamaBioskop()+" pada hari "+jadwalhari+" seharga Rp. "+ticket.getHarga());
                    bioskop.setSaldoKas(bioskop.getSaldoKas()+ticket.getHarga());
                    return ticket;

                }else if(rating.equals("Remaja")){
                    if(umur>=13){
                        System.out.println(nama+" telah membeli tiket "+judul+" jenis "+jenisFilm+" di "+bioskop.getNamaBioskop()+" pada hari "+jadwalhari+" seharga Rp. "+ticket.getHarga());
                        bioskop.setSaldoKas(bioskop.getSaldoKas()+ticket.getHarga());
                        return ticket;
                    }
                    else{
                        System.out.println(nama+" masih belum cukup umur untuk menonton "+judul+" dengan rating "+rating);
                        return null;
                    }
                }
                else if(rating.equals("Dewasa")){
                    if(umur>=17){
                        System.out.println(nama+" telah membeli tiket "+judul+" jenis "+jenisFilm+" di "+bioskop.getNamaBioskop()+" pada hari "+jadwalhari+" seharga Rp. "+ticket.getHarga());
                        bioskop.setSaldoKas(bioskop.getSaldoKas()+ticket.getHarga());
                        return ticket;
                    }
                    else{
                        System.out.println(nama+" masih belum cukup umur untuk menonton "+judul+" dengan rating "+rating);
                        return null;
                    }
                }
            }
}
        System.out.println("Tiket untuk film "+judul+" jenis "+jenisFilm+" dengan jadwal "+jadwalhari+" tidak tersedia di "+bioskop.getNamaBioskop());
        return null;
    }




    public void findMovie(Theater bioskop, String film){
        Movie[] movie = bioskop.getMovies(); 
        int panjang = movie.length;
        for(int a=0; a<panjang;a++){
            if(film.equals(movie[a].getJudul())){
                System.out.print(movie[a]);

                return ;
            }
        }
        System.out.println("Film "+film +" yang dicari "+getNama()+" tidak ada di bioskop "+bioskop.getNamaBioskop());
        return;
    }
}