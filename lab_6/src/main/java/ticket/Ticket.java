package ticket;
import movie.*;


public class Ticket {
	private Movie film;
	private String hari;
	private boolean is3D;
	private String jenisFilm;
	
	public Ticket(Movie film, String hari, boolean is3D){
		this.film = film;
		this.hari = hari;
		this.is3D = is3D;
		if(is3D){
			this.jenisFilm = "3 Dimensi";
		}
		else {
			this.jenisFilm = "Biasa";
		}
		
	}
	
	public Movie getFilm(){
		return film;
	}
	
	public void setFilm(Movie film){
		this.film = film;
	}
	
	public String getHari(){
		return hari;
	}
	
	public void setHari(String hari){
		this.hari = hari;
	}
	
	public boolean getIs3D(){
		return is3D;
	}
	
			
	public int getHarga(){
        int harga = 60000;
        if (this.hari.equals("Sabtu") || this.hari.equals("Minggu")){
            harga += 40000;
        }
        if (this.is3D){
            harga += harga/5;
        }
        return harga;
    }
	
	public String getJenisFilm(){
		return jenisFilm;
	}
	public String toString(){
        return "------------------------------------------------------------------\nFilm            : "+
            film.getJudul()+"\nJadwal Tayang   : "+hari+"\nJenis           : "+is3D+
            "\n------------------------------------------------------------------";
    }
}

