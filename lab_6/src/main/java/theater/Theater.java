package theater;
import movie.*;
import ticket.*;
import java.util.ArrayList;

public class Theater {
	private String namabioskop ;
	private int saldokas;
	private ArrayList<Ticket> ticket;
	private Movie[] movies;

	public Theater ( String namabioskop, int saldokas, ArrayList<Ticket> ticket, Movie[] movies){
		this.namabioskop = namabioskop;
		this.saldokas = saldokas;
		this.ticket = ticket;
		this.movies = movies;
	
	}
	
	public String getNamaBioskop(){
		return namabioskop;
	}
	
	public void setNamaBioskop(String namabioskop){
		this.namabioskop = namabioskop;
	}
	
	public int getSaldoKas(){
		return saldokas;
	}
	
	public void setSaldoKas(int saldokas){
		this.saldokas = saldokas;
	}
	
	public ArrayList<Ticket> getTicket(){
		return ticket;
	}
	
	public void setTicket(ArrayList<Ticket> ticket){
		this.ticket = ticket;
	}
	
	public Movie[] getMovies (){
		return movies;
	}
	
	public void setMovies(Movie[] movies){
		this.movies = movies;
	}
	
	public void printInfo(){
        System.out.println("------------------------------------------------------------------");
        System.out.println("Bioskop                 : "+namabioskop);
        System.out.println("Saldo Kas               : "+saldokas);
        System.out.println("Jumlah tiket tersedia   : "+ticket.size());
        System.out.print("Daftar Film tersedia    : ");
        for(int i=0; i<movies.length;i++){
            if(i==movies.length-1){
                System.out.println(movies[i].getJudul());
            }
            else{
                System.out.print(movies[i].getJudul()+", ");
            }
        }
        System.out.println("------------------------------------------------------------------");
    }

    public static void printTotalRevenueEarned(Theater[] bioskop){
        int totalpendapatan=0;
        for(Theater theater : bioskop){
            totalpendapatan+=theater.getSaldoKas();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. "+totalpendapatan);
        System.out.println("------------------------------------------------------------------");
        for(int i =0; i<bioskop.length; i++){
            if(i==bioskop.length-1){
                System.out.println("Bioskop     : "+bioskop[i].getNamaBioskop());
                System.out.println("Saldo Kas   : Rp. "+bioskop[i].getSaldoKas());
                System.out.println("");
                System.out.println("------------------------------------------------------------------");
            }
            else{
                System.out.println("Bioskop     : "+bioskop[i].getNamaBioskop());
                System.out.println("Saldo Kas   : Rp. "+bioskop[i].getSaldoKas());
                System.out.println("");
			}

		
			}
	}
}


