package character;

public class Monster extends character.Player{
	public Monster(String name,int hp){
		super(name,hp);
		this.hp = hp*2;
		this.tipe = "Monster";
		this.roar = roar();
	}
	
	public Monster(String name,int hp,String teriak){
		super(name,hp);
		this.hp = hp*2;
		this.tipe = "Monster";
		this.roar = roar(teriak);
	}
	public String roar(){	
		return "AAAAAAaaaAAAAAaaaAAAAAA";
	}
	public String roar(String teriak){
		return teriak;
	}
}
//  write Monster Class here