package character;
public class Magician extends character.Player{
	public Magician(String name,int hp){
	super(name,hp);
	this.tipe = "Magician";
	}
	
	public String burn(Player dibakar){
		if(dibakar.getMati()){
			dibakar.setBakar();
			return "Nyawa " + dibakar.getNama() + " " + dibakar.getHp() +" \ndan Matang";
		}else if(dibakar.getTipe().equals("Magician")){
			dibakar.setHp(dibakar.getHp() - 20);
			if(dibakar.getHp() <= 20){
				dibakar.setHp(0);
				dibakar.setBakar();
				dibakar.setMati();
				return "Nyawa " + dibakar.getNama() + " " + dibakar.getHp() +" \ndan Matang";
			}
			
			else{
				return "Nyawa " + dibakar.getNama() + " " + dibakar.getHp();
			}
			
		}else{
			dibakar.setHp(dibakar.getHp() - 10);
			if(dibakar.getHp() <= 10){
				dibakar.setHp(0);
				dibakar.setBakar();
				dibakar.setMati();
				return "Nyawa " + dibakar.getNama() + " " + dibakar.getHp() +" \ndan Matang";
			}
			
			else{
				return "Nyawa " + dibakar.getNama() + " " + dibakar.getHp();
			}
		}
	}
}
//  write Magician Class here