import java.util.Scanner;

public class RabbitHouse{
	public static void main (String args[]){
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nama kelinci yang anda inginkan :");
		String nama = input.nextLine();
		int huruf = nama.split(" ")[1].length();
		if (huruf > 10){
			System.out.println("Nama kelinci tidak boleh lebih dari 10 karakter");
			System.exit(0);
		}
		System.out.println(Total(huruf));
		
	}
	
	public static int Total (int huruf){
		int awal = 1;
		if (huruf <= 1){
			return 1 ;
		}
		else{
			awal += huruf *(Total(huruf-1));
			return awal ;
		
		}
	}
	

}

