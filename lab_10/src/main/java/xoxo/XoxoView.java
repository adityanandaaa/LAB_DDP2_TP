package xoxo;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.Dimension;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {
    private JFrame mainFrame;
    private JFrame warning;
    private JPanel inputPanel;
    private JLabel msglabel;
    private JLabel keylabel;
    private JLabel seedlabel;
    private JLabel loglabel;
    private JTextField seedField;
	
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    
    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //TODO: Construct your GUI here
		this.mainFrame = new JFrame("LAB10");
        this.mainFrame.setLayout(new GridLayout(1,2));
        this.mainFrame.setSize(new Dimension(1200, 650));
        this.mainFrame.setResizable(true);
        this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //textbuttonPanel
        JPanel textAndButtonPanel = new JPanel();
        textAndButtonPanel.setLayout(new GridLayout(2, 1));
        //texbuttonpanel- -> textpanel
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new GridLayout(6,1));

        this.messageField = new JTextField();
        this.keyField = new JTextField();
        this.seedField = new JTextField();

        textPanel.add(new JLabel("Message"));
        textPanel.add(this.messageField);
        textPanel.add(new JLabel("Key"));
        textPanel.add(this.keyField);
        textPanel.add(new JLabel("Seed"));
        textPanel.add(this.seedField);

        textAndButtonPanel.add(textPanel);

        //texbuttonpanel- -> butttonPanel
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1,3));

        this.encryptButton = new JButton("Encrypt Message");
        this.decryptButton = new JButton("Decrypt Message");
        JButton clearLog  = new JButton("Clear Log");


        clearLog.addActionListener(new ActionListener()
        {
			    public void actionPerformed(ActionEvent e)
          {
              clearLog();
          }
        });

        buttonPanel.add(this.encryptButton);
        buttonPanel.add(this.decryptButton);
        buttonPanel.add(clearLog);

        textAndButtonPanel.add(buttonPanel);

        //JPanel for log
        JPanel logPanel = new JPanel();
        this.logField = new JTextArea(20,50);
       
        this.logField.setLineWrap(true);
        this.logField.setWrapStyleWord(true);
        this.logField.setEditable(false);
        JScrollPane logScrollbar = new JScrollPane(this.logField, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        logPanel.add(logScrollbar, BorderLayout.PAGE_START);

        this.mainFrame.add(textAndButtonPanel);
        this.mainFrame.add(logPanel);
        this.mainFrame.setVisible(true);
	
	}
	

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
	
	public void clearLog(){
        this.logField.setText(null);
    }

    public JFrame getFrame(){
        return this.mainFrame;
    }
	
	public void setWarningMessage(String warn){
        JOptionPane.showMessageDialog(this.warning, warn,"Error",JOptionPane.WARNING_MESSAGE);
    }
}