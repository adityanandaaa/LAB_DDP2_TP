package xoxo.crypto;

import xoxo.exceptions.*;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     * 
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException{
        for (int i = 0; i<kissKeyString.length(); i++){
			if ((kissKeyString.charAt(i)<64 && kissKeyString.charAt(i)>90) || (kissKeyString.charAt(i) < 97 && kissKeyString.charAt(i)>122)) throw new InvalidCharacterException("String Kiss Key hanya boleh mengandung huruf A-Z, a-z, dan karakter @");
		}
		if  (kissKeyString.length() > 28) throw new KeyTooLongException("Panjang key maksimal 28 karakter");
			this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) {
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if ... <complete this>
     */
    public XoxoMessage encrypt(String message, int seed) {
        //TODO: throw RangeExceededException for seed requirements
		if (seed < 0 || seed > 36) throw new RangeExceededException("Seed merupakan angka di antara 0-36 (inclusive)"); 
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     * 
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if ... <complete this>
     * @throws InvalidCharacterException if ... <complete this>
     */
    private String encryptMessage(String message) {
        //TODO: throw SizeTooBigException for message requirements
		if (message.getBytes().length*8 >= 10000) throw new SizeTooBigException("Ukuran message maksimal 10 KBit");
        final int length = message.length();
        String encryptedMessage = "";
        for (int i = 0; i < length; i++) {
			if ((this.kissKey.keyAt(i)<64 && this.kissKey.keyAt(i)>90) || (this.kissKey.keyAt(i) < 97 && this.kissKey.keyAt(i)>122)) throw new InvalidCharacterException("String Kiss Key hanya boleh mengandung huruf A-Z, a-z, dan karakter @");
            //TODO: throw InvalidCharacterException for message requirements
            int m = message.charAt(i);
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }
}

