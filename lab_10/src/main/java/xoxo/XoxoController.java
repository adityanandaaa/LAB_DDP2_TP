package xoxo;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import xoxo.util.*;
import xoxo.crypto.*;
import xoxo.key.*;
import xoxo.exceptions.*;
import xoxo.XoxoView;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {
	
    
	/**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;
	
    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        //TODO: Write your code for logic and everything here
		ActionListener encryptListener = new ActionListener(){
			
			public void actionPerformed(ActionEvent a){
				String message = gui.getMessageText();
				String key = gui.getKeyText();
				String seed = gui.getSeedText();
				XoxoEncryption encr = null;
				String hasil = "";
				XoxoMessage xoxomessage;
				HugKey hugkey;
				
				try{
					encr = new XoxoEncryption(key);	
				}
				
				catch (KeyTooLongException e){
					gui.setWarningMessage(e.getMessage());
				} catch (Exception e){
					gui.setWarningMessage(e.getMessage());
				}
				
				if(message.equals("")){
					gui.setWarningMessage("Please enter the message");
				}
				
				else if(seed.equals("")){
					try{
						xoxomessage = encr.encrypt(message);
						hasil = xoxomessage.getEncryptedMessage();
						hugkey = xoxomessage.getHugKey();
						gui.appendLog(String.format("Encryption Complete%nEncryption result: %s%nHugKey: %s", hasil, hugkey.getKeyString()));
					}
					
					catch (InvalidCharacterException e){
						gui.setWarningMessage(e.getMessage());
					} catch (RangeExceededException e){
						gui.setWarningMessage(e.getMessage());
					} catch (SizeTooBigException e){
						gui.setWarningMessage(e.getMessage());
					} catch (Exception e){
						gui.setWarningMessage(e.getMessage());
					}
				}
				
				else{
					try{
						xoxomessage = encr.encrypt(message, Integer.parseInt(seed));
						hasil = xoxomessage.getEncryptedMessage();
						hugkey = xoxomessage.getHugKey();
						gui.appendLog(String.format("Encryption Complete%Encryption result: %s", hasil));
					}
					catch (InvalidCharacterException e){
						gui.setWarningMessage(e.getMessage());
					} catch (RangeExceededException e){
						gui.setWarningMessage(e.getMessage());
					} catch (SizeTooBigException e){
						gui.setWarningMessage(e.getMessage());
					} catch (Exception e){
						gui.setWarningMessage(e.getMessage());
					}
				}
				makingNewFile(hasil, hasil, "encrypt");
			}
		};
		
		ActionListener decryptListener = new ActionListener(){
			
			public void actionPerformed(ActionEvent action){
				String message = gui.getMessageText();
				String key = gui.getKeyText();
				String seed = gui.getSeedText();
				String hasil = "";
				XoxoDecryption decryption =  new XoxoDecryption(key);
				
				if(message.equals("")){
					gui.setWarningMessage("Please enter the message");
				}
				
				else if(seed.equals("")){
					hasil = decryption.decrypt(message, 18);
					gui.appendLog(String.format("Decrytion Complete%nDecryption result: %s", hasil));
				}
				
				else{
					hasil = decryption.decrypt(message, Integer.parseInt(seed));
					gui.appendLog(String.format("Decryption Complete%nDecryption result: %s", hasil));
				}
				
				makingNewFile(hasil, hasil, "decrypt");
			}
		};
		gui.setEncryptFunction(encryptListener);
		gui.setDecryptFunction(decryptListener);
				
    }
	
	void makingNewFile(String name, String outputString, String status){
		String extension = "";
		if (status.equalsIgnoreCase("encrypt")){
			extension = ".enc";
		}
		else {
			extension = ".txt";
		}
		
		FileWriter writer;
		try{
			File output = new File(name);
			output.createNewFile();
			writer = new FileWriter(output);
			writer.write(String.format("Message: %s",outputString));
			writer.flush();
			writer.close();
		}
		
		catch (IOException e){
			gui.setWarningMessage(e.getMessage());
		}
	}

    //TODO: Create any methods that you want
}